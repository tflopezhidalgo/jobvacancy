require 'spec_helper'

describe JobOffer do
  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.new(location: 'a location')
      end
    end

    it 'should be valid when title is not blank' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer).to be_valid
    end
  end

  describe 'has required experience' do
    it 'should has 3 of required experience' do
      job_offer = described_class.new(title: 'testing', required_experience: 3)

      expect(job_offer.required_experience).to eq 3
    end

    it 'should has 7 of required experience' do
      job_offer = described_class.new(title: 'testing', required_experience: 7)

      expect(job_offer.required_experience).to eq 7
    end

    it 'should has 0 of required experience if required_experience is not valid' do
      job_offer = described_class.new(
        title: 'testing',
        required_experience: 'not a valid experience'
      )

      expect(job_offer.required_experience).to eq 0
    end
  end
end
