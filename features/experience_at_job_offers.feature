Feature: Experience at offer creation

  Background:
    Given I am logged in as job offerer

  Scenario: Set required experience at job offer creation
    Given I create a new offer with "3" years as required experience 
    And I set offer as active
    When I access the offers list page
    Then I should see a offer with "3" as required experience 

  Scenario: Null experience is considered as not especified
    Given I create a new offer with "0" years as required experience 
    And I set offer as active
    When I access the offers list page
    Then I should see a offer with "Not especified" as required experience 

  Scenario: Negative experience is not allowed
    Given I create a new offer with "-1" years as required experience 
    Then I should see a error with the message "required experience should be positive"


